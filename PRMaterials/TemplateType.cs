﻿namespace PRMaterials
{
    /// <summary>Type of template</summary>
    public enum TemplateType
    {
        Unknown,

        Sound,

        Effect,

        Decal
    }
}
