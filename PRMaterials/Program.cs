﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;

namespace PRMaterials
{
    class Program
    {
        static void Main(string[] args)
        {
            // Parsing Arguments
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(Run);

            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        /// <summary>
        /// Runs the Program
        /// </summary>
        /// <param name="o"></param>
        private static void Run(Options o)
        {
            if (o.ToJson)
            {
                FromCon(o);
            }
            else
            {
                // TODO Add Config
                Console.WriteLine("Reading templates...");
                var templates = new Templates();
                templates.FindTemplates(new[] {"../", "../../objects/effects/"});

                Console.WriteLine("Writing files...");
                FromJson(o, templates);
            }
            // TODO Add option to validate con files
        }

        /// <summary>
        /// Reads Materials and Cells from Json and writes to Con
        /// </summary>
        /// <param name="o"></param>
        /// <param name="templates"></param>
        private static void FromJson(Options o, Templates templates)
        {
            var materials = new MaterialManager();
            var cells = new CellManager();

            using var materialsStream = File.Open( "materials.json", FileMode.OpenOrCreate);
            using var materialsReader = new StreamReader( materialsStream );
            using var cellsStream = File.Open( "cells.json", FileMode.OpenOrCreate);
            using var cellsReader = new StreamReader( cellsStream );

            materials.ReadJson(materialsReader);
            cells.ReadJson(cellsReader, materials.Materials.ToList());

            if (o.Materials)
            {
                WriteMaterialsCon(materials);
            }
            else if (o.Cells)
            {
                WriteEffectiveCellsCon(materials, cells, templates);
            }
            else
            {
                WriteMaterialsCon(materials);
                WriteEffectiveCellsCon(materials, cells, templates);
            }
        }

        /// <summary>
        /// Reads Materials and Cells from Con Files and writes to Json
        /// </summary>
        /// <param name="o"></param>
        private static void FromCon(Options o)
        {
            var materials = new MaterialManager();
            var cells = new CellManager();

            using var materialsStream = File.Open( "materialManagerDefine.con", FileMode.OpenOrCreate);
            using var materialsReader = new StreamReader(materialsStream);
            using var cellsStream = File.Open( "materialManagerSettings.con", FileMode.OpenOrCreate);
            using var cellsReader = new StreamReader( cellsStream );

            materials.ReadCon(materialsReader);
            cells.ReadCon(cellsReader);

            if (o.Materials)
            {
                WriteMaterialsJson(materials);
            }
            else if (o.Cells)
            {
                WriteCellsJson(cells);
            }
            else
            {
                WriteMaterialsJson(materials);
                WriteCellsJson(cells);
            }
        }

        /// <summary>
        /// Write Materials to Con
        /// </summary>
        /// <param name="materials"></param>
        private static void WriteMaterialsCon(MaterialManager materials)
        {
            using var stream = File.Open("materialManagerDefine.con", FileMode.OpenOrCreate);
            using var writer = new StreamWriter(stream);
            stream.SetLength(0);
            ConWriter.WriteMaterials(materials.Materials, writer);
        }

        /// <summary>
        /// Write effective cells to con
        /// </summary>
        /// <param name="materials"></param>
        /// <param name="cells"></param>
        /// <param name="templates"></param>
        private static void WriteEffectiveCellsCon(MaterialManager materials, CellManager cells,
            Templates templates)
        {
            var effectiveCells = new List<KeyValuePair<CellKey, Cell>>();

            foreach (var attacker in materials.Materials)
            {
                foreach (var defender in materials.Materials)
                {
                    var cellKey = new CellKey(attacker.Id, defender.Id);
                    var cell = cells.FindEffectiveCell(attacker, defender, materials);
                    if (cell == null) continue;

                    // If default is attacking and damage mod is smaller then 0
                    // We want every attacker to deal 0 damage unless defined otherwise
                    if (attacker.Id == 0 && cell.DamageMod < 0)
                    {
                        cell.DamageMod = 0;
                        foreach (var m in materials.Materials)
                        {
                            var key = new CellKey(m.Id, defender.Id);
                            var c = cells.FindEffectiveCell(m, defender, materials);
                            if (c == null)
                            {
                                // Create a new cell
                                AddCell(cell, m, defender, key);
                            }
                        }
                    }
                    else
                    {
                        AddCell(cell, attacker, defender, cellKey);
                    }
                }
            }

            using var stream = File.Open("materialManagerSettings.con", FileMode.OpenOrCreate);
            using var writer = new StreamWriter(stream);
            stream.SetLength(0);
            ConWriter.WriteCells(effectiveCells, writer);

            void AddCell(Cell? cell, Material? attacker, Material? defender, CellKey cellKey)
            {
                if (!cell.ValidateCellTemplates(templates, out var badTemplates))
                {
                    var errors = string.Join(", ", badTemplates);
                    Console.WriteLine($"Cell {attacker.Id}:{defender.Id} has invalid templates: {errors}");
                }

                effectiveCells.Add(new KeyValuePair<CellKey, Cell>(cellKey, cell));
            }
        }

        /// <summary>
        /// Write Materials to Json
        /// </summary>
        /// <param name="materials"></param>
        private static void WriteMaterialsJson(MaterialManager materials)
        {
            using var stream = File.Open("materials.json", FileMode.OpenOrCreate);
            using var writer = new StreamWriter(stream);
            stream.SetLength(0);
            materials.WriteJson(writer);
        }

        /// <summary>
        /// Write Cells to Json
        /// </summary>
        /// <param name="cells"></param>
        private static void WriteCellsJson(CellManager cells)
        {
            using var stream = File.Open("cells.json", FileMode.OpenOrCreate);
            using var writer = new StreamWriter(stream);
            stream.SetLength(0);
            cells.WriteJson(writer);
        }
    }
}
