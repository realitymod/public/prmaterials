﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PRMaterials
{
    /// <summary>Manager for all defined materials</summary>
    public class MaterialManager
    {
        /// <summary>Dictionary of the Materials</summary>
        private readonly IDictionary<int, Material> _materials = new Dictionary<int, Material>();

        /// <summary>List of all the materials</summary>
        public IEnumerable<Material> Materials => _materials.Values;

        /// <summary>
        /// Gets Material
        /// </summary>
        /// <param name="materialId">Id of the material</param>
        /// <returns>Material or NULL if none found</returns>
        public Material? GetMaterial(int materialId)
        {
            if (_materials.TryGetValue(materialId, out var material))
            {
                return material;
            }

            return null;
        }


        /// <summary>
        /// Writes materials to a json file
        /// </summary>
        /// <param name="jsonWriter">Writer to write json file</param>
        public void WriteJson(TextWriter jsonWriter)
        {
            var serializer = new JsonSerializer { Formatting = Formatting.Indented };
            serializer.Converters.Add(new StringEnumConverter());
            serializer.Serialize( jsonWriter, Materials.OrderBy( x => x.Id ) );
        }

        /// <summary>
        /// Reads materials from a json file
        /// </summary>
        /// <param name="jsonReader">Reader to read json file</param>
        public void ReadJson(StreamReader jsonReader)
        {
            var serializer = new JsonSerializer { Formatting = Formatting.Indented };
            serializer.Converters.Add(new StringEnumConverter());
            var jsonMaterials = serializer.Deserialize<IEnumerable<Material>>( new JsonTextReader(jsonReader) ) 
                                ?? throw new InvalidOperationException("Failed reading materials");

            foreach (var material in jsonMaterials)
            {
                _materials[material.Id] = material;
            }
        }

        /// <summary>
        /// Reads Materials of a con file
        /// </summary>
        /// <param name="conReader">The reader for the con file</param>
        public void ReadCon(TextReader conReader)
        {

            string? line;
            var currentMaterialId = -1;
            while ((line = conReader.ReadLine()) != null) {
                // When it is not a Line defining a material
                if (!line.StartsWith( "Material.", StringComparison.InvariantCultureIgnoreCase )) continue;

                // Everything after Material. is the command and parameters
                var commandAndParam = line.Substring( "Material.".Length );

                var split = commandAndParam.Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

                // When not enough parts
                if (split.Length < 2) continue;

                // Command comes first
                var command = split[0].ToUpperInvariant();
                // Everything after that is parameter
                var parameter = string.Join( " ", split.Skip( 1 ) ).Trim( '"' );

                if (command == "ACTIVE") {
                    if (int.TryParse( parameter, out var id )) {
                        // create new Material
                        currentMaterialId = id;
                        this._materials[id] = new Material { Id = id };
                    }
                }
                else {
                    var property = typeof( Material ).GetProperties().FirstOrDefault( x => x.Name.ToUpperInvariant() == command );
                    if (property == null) continue;

                    var material = this._materials[currentMaterialId];

                    object? value;
                    if (property.PropertyType == typeof( bool )) {
                        value = parameter == "1";
                    }
                    else if (property.PropertyType == typeof(MaterialType))
                    {
                        Enum.TryParse(typeof(MaterialType), parameter, out value);
                    }
                    else {
                        value = Convert.ChangeType( parameter, property.PropertyType );
                    }

                    property.SetValue( material, value );
                }
            }
        }
    }
}
