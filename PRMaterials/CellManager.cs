﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace PRMaterials
{
    /// <summary>Manager for all the cells</summary>
    public class CellManager
    {
        /// <summary>The defined cells</summary>
        private Dictionary<CellKey, Cell> _cells = new Dictionary<CellKey, Cell>();

        
        /// <summary>
        /// Finds the effective cell considering inheritance between two materials
        /// </summary>
        /// <param name="attacker">Attacker material</param>
        /// <param name="defender">Defender material</param>
        /// <param name="materialManager">Material manager to find base materials</param>
        /// <returns>Effective cell between the materials or NULL if none defined</returns>
        public Cell? FindEffectiveCell(Material attacker, Material defender, MaterialManager materialManager)
        {
            var cell = FindCell(attacker.Id, defender.Id);
            // First check if defender inherits anything
            if (defender.BaseMaterial != null)
            {
                var baseDefender = materialManager.GetMaterial(defender.BaseMaterial.Value);
                var baseCell = FindEffectiveCell(attacker, baseDefender, materialManager);
                cell = OverrideCell(baseCell, cell);
            }

            // Then check the attacker
            if (attacker.BaseMaterial != null)
            {
                var baseAttacker = materialManager.GetMaterial(attacker.BaseMaterial.Value);
                var baseCell = FindEffectiveCell(baseAttacker, defender, materialManager);
                cell = OverrideCell(baseCell, cell);
            }

            return cell;
        }

        /// <summary>
        /// Merges two Cells
        /// </summary>
        /// <param name="baseCell">Base Cell</param>
        /// <param name="derivedCell">Cell that override base</param>
        /// <returns>Base cell with values overriden </returns>
        private Cell? OverrideCell(Cell? baseCell, Cell? derivedCell)
        {
            if (baseCell == null)
            {
                return derivedCell;
            }

            if (derivedCell == null)
            {
                return baseCell;
            }

            return baseCell.Override(derivedCell);
        }

        /// <summary>
        /// Finds cell between two materials
        /// </summary>
        /// <param name="attacker">The attacking material</param>
        /// <param name="defender">The defending material</param>
        /// <returns>Cell between materials, NULL if none present</returns>
        private Cell? FindCell(int attacker, int defender)
        {
            var key = new CellKey(attacker, defender);
            
            if (_cells.TryGetValue(key, out var cell))
            {
                return cell;
            }

            return null;
        }

        /// <summary>
        /// Writes materials to a json file
        /// </summary>
        /// <param name="jsonWriter">Writer to write json file</param>
        public void WriteJson(TextWriter jsonWriter)
        {
            var serializer = new JsonSerializer
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore
            };
            serializer.Converters.Add(new CellDictionaryConverter());
            serializer.Serialize( jsonWriter, _cells);
        }

        /// <summary>
        /// Reads cells from a json file
        /// </summary>
        /// <param name="jsonReader">Reader to read json file</param>
        /// <param name="materials">Available materials</param>
        public void ReadJson(TextReader jsonReader, IReadOnlyCollection<Material> materials)
        {
            var serializer = new JsonSerializer
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore
            };
            serializer.Converters.Add(new CellDictionaryConverter());
            var cells = serializer.Deserialize<Dictionary<CellKey, Cell>>( new JsonTextReader(jsonReader) );

            if (cells is null) throw new InvalidOperationException("Failed reading cells");

            _cells = new Dictionary<CellKey, Cell>();

            foreach (var cell in cells)
            {
                var key = cell.Key;
                var value = cell.Value;

                var keyCells = GetEffectiveKeys(key, materials);
                foreach (var c in keyCells)
                {
                    _cells[c] = value.Clone();
                }
            }
        }

        /// <summary>Gets the effective cell Keys for a key</summary>
        private IEnumerable<CellKey> GetEffectiveKeys(CellKey key, IReadOnlyCollection<Material> materials)
        {
            var attackerAll = key.Attacker < 0;
            var defenderAll = key.Defender < 0;
            var attackerExists = attackerAll || materials.Any(x => x.Id == key.Attacker);
            if (!attackerExists)
            {
                throw new InvalidOperationException($"Attacker with Id {key.Attacker} does not exist");
            }
            var defenderExists = defenderAll || materials.Any(x => x.Id == key.Defender);
            if (!defenderExists)
            {
                throw new InvalidOperationException($"Defender with Id {key.Defender} does not exist");
            }

            if (attackerAll && defenderAll)
            {
                throw new InvalidOperationException("Cannot specify cell for ALL vs ALL");
            }
            if (attackerAll)
            {
                foreach (var m in materials)
                {
                    yield return new CellKey(m.Id, key.Defender);
                }
                yield break;
            }

            if (defenderAll)
            {
                foreach (var m in materials)
                {
                    yield return new CellKey(key.Attacker, m.Id);
                }
                yield break;
            }
            
            yield return key;
        }

        /// <summary>
        /// Reads the cells from .con file
        /// </summary>
        /// <param name="conReader"></param>
        public void ReadCon(TextReader conReader)
        {
            string? line;
            Cell? currentCell = null;
            while ((line = conReader.ReadLine()) != null) {
                if (!line.StartsWith( "MaterialManager.", StringComparison.InvariantCultureIgnoreCase )) continue;

                // Everything after Material. is the command and parameters
                var commandAndParam = line.Substring( "MaterialManager.".Length );

                var split = commandAndParam.Split( new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries );

                // When not enough parts
                if (split.Length < 2) continue;

                // Command comes first
                var command = split[0].ToUpperInvariant();

                // When new cell gets created
                if (command == "CREATECELL") {
                    if (!int.TryParse( split[1], out var attacker )) continue;
                    if (!int.TryParse( split[2], out var defender )) continue;

                    var key = new CellKey(attacker, defender);
                    currentCell = new Cell();
                    _cells[key] = currentCell;
                    continue;
                }

                // No Cell created yet
                if (currentCell == null) continue;

                if (command == "DAMAGEMOD") {
                    if(!decimal.TryParse(split[1], out var damageMod)) continue;
                    currentCell.DamageMod = damageMod;
                }
                else if (command == "SETSOUNDTEMPLATE")
                {
                    if (!int.TryParse(split[1], out var index)) continue;
                    var template = split[2];

                    currentCell.SetSound(template, index);
                }
                else if (command == "SETEFFECTTEMPLATE")
                {
                    if (!int.TryParse(split[1], out var index)) continue;
                    var template = split[2];

                    currentCell.SetEffect(template, index);
                }
                else if(command == "SETDECALTEMPLATE")
                {
                    if (!int.TryParse(split[1], out var index)) continue;
                    var template = split[2];

                    currentCell.SetDecal(template, index);
                }
                else
                {
                    throw new InvalidOperationException("Unknown materialmanager command");
                }
            }
        }
    }

    /// <summary>Key for looking up a cell between two materials</summary>
    public readonly struct CellKey : IComparable<CellKey>
    {
        /// <summary>Ctor</summary>
        public CellKey(int attacker, int defender)
        {
            Attacker = attacker;
            Defender = defender;
        }

        /// <summary>The Material attacking</summary>
        public int Attacker { get; }

        /// <summary>The Material defending</summary>
        public int Defender { get;  }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{Attacker}:{Defender}";
        }

        /// <inheritdoc />
        public int CompareTo(CellKey other)
        {
            var attackerComparison = Attacker.CompareTo(other.Attacker);
            if (attackerComparison != 0) return attackerComparison;
            return Defender.CompareTo(other.Defender);
        }
    }

    /// <summary>Converter to deserialize the dictionary</summary>
    public class CellDictionaryConverter : JsonConverter<Dictionary<CellKey, Cell>?>
    {
        /// <inheritdoc />
        public override bool CanWrite => false;

        /// <inheritdoc />
        public override void WriteJson(JsonWriter writer, Dictionary<CellKey, Cell>? value, JsonSerializer serializer)
        {
            throw new NotSupportedException();
        }

        /// <inheritdoc />
        public override Dictionary<CellKey, Cell>? ReadJson(JsonReader reader, Type objectType, Dictionary<CellKey, Cell>? existingValue, bool hasExistingValue,
            JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            var intermediateDictionary = new Dictionary<string, Cell>();
            serializer.Populate(reader, intermediateDictionary);

            var finalDictionary = new Dictionary<CellKey, Cell>();
            foreach (var (keyString, cell) in intermediateDictionary)
            {
                SplitKeyString(keyString, out var attackerPart, out var defenderPart);

                var attacker = ParsePart(attackerPart);
                var defender = ParsePart(defenderPart);
                var key = new CellKey(attacker, defender);
                finalDictionary.Add(key, cell);
            }

            return finalDictionary;

            void SplitKeyString(ReadOnlySpan<char> keyString, out ReadOnlySpan<char> attacker,
                out ReadOnlySpan<char> defender)
            {
                var i = keyString.IndexOf(':');
                attacker = keyString[..i];
                defender = keyString[(i + 1)..];
            }

            int ParsePart(ReadOnlySpan<char> part)
            {
                int attacker;
                var isAllMaterial = part.Equals("ALL", StringComparison.OrdinalIgnoreCase);
                if (isAllMaterial)
                {
                    attacker = -1;
                }
                else
                {
                    attacker = int.Parse(part);
                }

                return attacker;
            }
        }
    }
}

