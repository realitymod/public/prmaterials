﻿using CommandLine;

namespace PRMaterials
{
    /// <summary>Command line options</summary>
    public class Options
    {

        /// <summary>Re-create Json files</summary>
        [Option("toJson", Default = false, HelpText = "Re-create Json files from con files")]
        public bool ToJson { get; set; }

        /// <summary>Only do materials</summary>
        [Option("materials", Default = false, HelpText = "Only do materials", SetName = "Cells")]
        public bool Materials { get; set; }

        /// <summary>Only do cells</summary>
        [Option("cells", Default = false, HelpText = "Only do cells", SetName = "Materials")]
        public bool Cells { get; set; }
    }
}
