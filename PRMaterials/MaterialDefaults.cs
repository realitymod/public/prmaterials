﻿namespace PRMaterials
{
    /// <summary>Class holding all the default values for materials</summary>
    public static class MaterialDefaults
    {
        /// <summary>Damage loss</summary>
        public const decimal DamageLoss = -1;

        /// <summary>Min damage loss</summary>
        public const decimal MinDamageLoss = 0;

        /// <summary>Max damage loss</summary>
        public const decimal MaxDamageLoss = 100_000;

        /// <summary>Elasticity</summary>
        public const decimal Elasticity = 0;

        /// <summary>Friction</summary>
        public const decimal Friction = 1;

        /// <summary>Flag if material is water</summary>
        public const bool HasWaterPhysics = false;

        /// <summary>Flag if material is barbwire (deals damage when touching)</summary>
        public const bool IsBarbwire = false;

        /// <summary>Flag if material is one sided</summary>
        public const bool IsOneSided = false;

        /// <summary>Flag if material is see through</summary>
        public const bool IsSeeThrough = false;

        /// <summary>Flag if material should always be penetrated even by materials that dont penetrate</summary>
        public const bool OverrideNeverPenetrate = false;

        /// <summary>Deviation added when penetrating</summary>
        public const decimal PenetrationDeviation = 0.1m;

        /// <summary>Collision hardness</summary>
        public const decimal ProjectileCollisionHardness = 0;

        /// <summary>Resistance</summary>
        public const decimal Resistance = 0.01m;
    }
}
