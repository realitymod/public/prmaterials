﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PRMaterials
{
    /// <summary>A cell in the material settings</summary>
    public class Cell
    {
        /// <summary>The Damage modifier</summary>
        public decimal? DamageMod { get; set; }

        /// <summary>Default Effect</summary>
        public string? DefaultEffect { get; set; }

        /// <summary>Default Decal</summary>
        public string? DefaultDecal { get; set; }

        /// <summary>Default Sound</summary>
        public string? DefaultSound { get; set; }

        /// <summary>Extra effect 1</summary>
        /// <remarks>
        /// When attacking is projectile: RicochetEffect
        /// When attacking is misc: CrouchEffect
        /// When attacking is vehicle: RollEffect
        /// When attacking is ground: None
        /// </remarks>
        public string? Effect1 { get; set; }

        /// <summary>Extra effect 2</summary>
        /// <remarks>
        /// When attacking is projectile: ExitEffect
        /// When attacking is misc: ProneEffect
        /// When attacking is vehicle: SkidEffect
        /// When attacking is ground: None
        /// </remarks>
        public string? Effect2 { get; set; }

        /// <summary>Extra effect 3</summary>
        /// <remarks>
        /// When attacking is projectile: None
        /// When attacking is misc: SprintEffect
        /// When attacking is vehicle: None
        /// When attacking is ground: None
        /// </remarks>
        public string? Effect3 { get; set; }

        /// <summary>Extra effect 4</summary>
        /// <remarks>
        /// When attacking is projectile: None
        /// When attacking is misc: DeathImpactEffect
        /// When attacking is vehicle: None
        /// When attacking is ground: None
        /// </remarks>
        public string? Effect4 { get; set; }

        /// <summary>Extra effect 1</summary>
        /// <remarks>
        /// When attacking is projectile: RicochetSound
        /// When attacking is misc: CrouchSound
        /// When attacking is vehicle: RollSound
        /// When attacking is ground: None
        /// </remarks>
        public string? Sound1 { get; set; }

        /// <summary>Extra effect 2</summary>
        /// <remarks>
        /// When attacking is projectile: ExitSound
        /// When attacking is misc: ProneSound
        /// When attacking is vehicle: SkidSound
        /// When attacking is ground: None
        /// </remarks>
        public string? Sound2 { get; set; }

        /// <summary>Extra effect 3</summary>
        /// <remarks>
        /// When attacking is projectile: None
        /// When attacking is misc: SprintSound
        /// When attacking is vehicle: None
        /// When attacking is ground: None
        /// </remarks>
        public string? Sound3 { get; set; }

        /// <summary>Extra effect 4</summary>
        /// <remarks>
        /// When attacking is projectile: None
        /// When attacking is misc: DeathImpactSound
        /// When attacking is vehicle: None
        /// When attacking is ground: None
        /// </remarks>
        public string? Sound4 { get; set; }

        /// <summary>Decal for ricochets</summary>
        public string? RicochetDecal { get; set; }

        /// <summary>Decal for projectiles exiting</summary>
        public string? ExitDecal { get; set; }

        /// <summary>
        /// Checks if all templates assigned to this cell are valid
        /// </summary>
        /// <param name="templates">Dictionary of all templates</param>
        /// <param name="badTemplates">Templates that are not valid</param>
        /// <returns>true if all valid</returns>
        public bool ValidateCellTemplates(Templates templates, out ICollection<string> badTemplates)
        {
            badTemplates = new List<string>();

            IsValid(DefaultEffect, TemplateType.Effect, badTemplates);
            IsValid(Effect1, TemplateType.Effect, badTemplates);
            IsValid(Effect2, TemplateType.Effect, badTemplates);
            IsValid(Effect3, TemplateType.Effect, badTemplates);
            IsValid(Effect4, TemplateType.Effect, badTemplates);

            IsValid(DefaultSound, TemplateType.Sound, badTemplates);
            IsValid(Sound1, TemplateType.Sound, badTemplates);
            IsValid(Sound2, TemplateType.Sound, badTemplates);
            IsValid(Sound3, TemplateType.Sound, badTemplates);
            IsValid(Sound4, TemplateType.Sound, badTemplates);

            IsValid(DefaultDecal, TemplateType.Decal, badTemplates);
            IsValid(ExitDecal, TemplateType.Decal, badTemplates);
            IsValid(RicochetDecal, TemplateType.Decal, badTemplates);

            return badTemplates.Count == 0;

            void IsValid(string? template, TemplateType expectedType, ICollection<string> issues)
            {
                // Empty, no need to check
                if (string.IsNullOrWhiteSpace(template)) return;
                var exists = templates.Exists(template, expectedType);
                if (exists != Templates.TemplateExists.Yes)
                {
                    issues.Add(template);
                }
            }
        }


        /// <summary>
        /// Sets effect into the cell
        /// </summary>
        /// <param name="template">Effect template</param>
        /// <param name="index">Index of the effect</param>
        public void SetEffect(in string template, in int index)
        {
            switch (index) {
                case 0:
                    this.DefaultEffect = template;
                    break;
                case 1:
                    this.Effect1 = template;
                    break;
                case 2:
                    this.Effect2 = template;
                    break;
                case 3:
                    this.Effect3 = template;
                    break;
                case 4:
                    this.Effect4 = template;
                    break;
                default:
                    throw new ArgumentOutOfRangeException( nameof( index ), "Can only set effect between 0 and 4" );
            }
        }

        /// <summary>
        /// Sets sound into cell
        /// </summary>
        /// <param name="template">Sound template</param>
        /// <param name="index">Index of the sound</param>
        public void SetSound(in string template, in int index)
        {
            switch (index) {
                case 0:
                    this.DefaultSound = template;
                    break;
                case 1:
                    this.Sound1 = template;
                    break;
                case 2:
                    this.Sound2 = template;
                    break;
                case 3:
                    this.Sound3 = template;
                    break;
                case 4:
                    this.Sound4 = template;
                    break;
                default:
                    throw new ArgumentOutOfRangeException( nameof( index ), "Can only set sound between 0 and 4" );
            }
        }

        /// <summary>
        /// Sets decal in to cell
        /// </summary>
        /// <param name="template">Decal template</param>
        /// <param name="index">Index of the decal</param>
        public void SetDecal(in string template, in int index)
        {
            switch (index) {
                case 0:
                    this.DefaultDecal = template;
                    break;
                case 1:
                    this.RicochetDecal = template;
                    break;
                case 2:
                    this.ExitDecal = template;
                    break;
                default:
                    throw new ArgumentOutOfRangeException( nameof( index ), "Can only set sound between 0 and 2" );
            }
        }

        /// <summary>
        /// Writes this cell to a con file
        /// </summary>
        /// <param name="conWriter"></param>
        public void WriteToCon(TextWriter conWriter)
        {
            const string materialManagerClass = "MaterialManager";

            if (this.DamageMod != null) conWriter.WriteLine( $"{materialManagerClass}.damageMod {this.DamageMod:####0.####}" );

            // Effects
            if (this.DefaultEffect != null) conWriter.WriteLine( $"{materialManagerClass}.setEffectTemplate 0 {this.DefaultEffect}" );
            if (this.Effect1 != null) conWriter.WriteLine( $"{materialManagerClass}.setEffectTemplate 1 {this.Effect1}" );
            if (this.Effect2 != null) conWriter.WriteLine( $"{materialManagerClass}.setEffectTemplate 2 {this.Effect2}" );
            if (this.Effect3 != null) conWriter.WriteLine( $"{materialManagerClass}.setEffectTemplate 3 {this.Effect3}" );
            if (this.Effect4 != null) conWriter.WriteLine( $"{materialManagerClass}.setEffectTemplate 4 {this.Effect4}" );

            // Decals
            if (this.DefaultDecal != null) conWriter.WriteLine( $"{materialManagerClass}.setDecalTemplate 0 {this.DefaultDecal}" );
            if (this.RicochetDecal != null) conWriter.WriteLine( $"{materialManagerClass}.setDecalTemplate 1 {this.RicochetDecal}" );
            if (this.ExitDecal != null) conWriter.WriteLine( $"{materialManagerClass}.setDecalTemplate 2 {this.ExitDecal}" );

            // Sounds
            if (this.DefaultSound != null) conWriter.WriteLine( $"{materialManagerClass}.setSoundTemplate 0 {this.DefaultSound}" );
            if (this.Sound1 != null) conWriter.WriteLine( $"{materialManagerClass}.setSoundTemplate 1 {this.Sound1}" );
            if (this.Sound2 != null) conWriter.WriteLine( $"{materialManagerClass}.setSoundTemplate 2 {this.Sound2}" );
            if (this.Sound3 != null) conWriter.WriteLine( $"{materialManagerClass}.setSoundTemplate 3 {this.Sound3}" );
            if (this.Sound4 != null) conWriter.WriteLine( $"{materialManagerClass}.setSoundTemplate 4 {this.Sound4}" );
        }

        /// <summary>
        /// Clones this cell with values from another cell
        /// </summary>
        /// <returns>New Cell with same values</returns>
        public Cell Clone()
        {
            var newCell = new Cell();
            var properties = newCell.GetType()
                .GetProperties( BindingFlags.Instance | BindingFlags.Public )
                .Where( x => x is { CanWrite: true, CanRead: true } );

            // TODO If too slow, then hardcode it
            foreach (var property in properties) {
                var thisValue = property.GetValue( this );
                property.SetValue( newCell, thisValue );
            }

            return newCell;
        }

        /// <summary>
        /// Overrides this cell with values from another cell
        /// </summary>
        /// <param name="derivedCell">The cell that overrides t his</param>
        /// <returns>New Cell with overriden values</returns>
        public Cell Override(Cell derivedCell)
        {
            var newCell = new Cell();
            var properties = newCell.GetType().GetProperties( BindingFlags.Instance | BindingFlags.Public )
                .Where( x => x.CanWrite && x.CanRead );

            // TODO If too slow, then hardcode it
            foreach (var property in properties) {
                var derivedValue = property.GetValue( derivedCell );
                var baseValue = property.GetValue( this );
                property.SetValue( newCell, derivedValue ?? baseValue );
            }

            return newCell;
        }
    }
}