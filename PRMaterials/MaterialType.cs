﻿namespace PRMaterials
{
    /// <summary>Type of a material</summary>
    public enum MaterialType
    {
        /// <summary>Ground Material</summary>
        Ground = 0,

        /// <summary>World Material</summary>
        World = 1,

        /// <summary>Projectile Material</summary>
        Projectiles = 2,

        /// <summary>Vehicles material</summary>
        Vehicles = 3,

        /// <summary>Misc Material</summary>
        Misc = 4,
    }
}
