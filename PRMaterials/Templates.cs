﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PRMaterials
{
    /// <summary>Datastructure to hold template information</summary>
    public class Templates
    {
        /// <summary>The templates</summary>
        private readonly IDictionary<string, TemplateType> _templates = new Dictionary<string, TemplateType>(StringComparer.OrdinalIgnoreCase);


        /// <summary>
        /// Searches for templates
        /// </summary>
        /// <param name="templateDirectories">The directories to search</param>
        public void FindTemplates(IEnumerable<string> templateDirectories)
        {
            // Pattern for matching template creation
            const string createTemplatePattern = @"ObjectTemplate\.create\s+(?<type>\w+)\s+(?<name>\w+)";
            // Reset found templates
            _templates.Clear();

            foreach (var root in templateDirectories)
            {
                var files = Directory.EnumerateFiles(root, "*.con", SearchOption.AllDirectories)
                    .Concat(Directory.EnumerateFiles(root, "*.tweak", SearchOption.AllDirectories));

                foreach (var file in files)
                {
                    using var fs = File.OpenRead(file);
                    using var reader = new StreamReader(fs);

                    var line = reader.ReadLine();
                    while (line != null)
                    {
                        var m = Regex.Match(line, createTemplatePattern, RegexOptions.IgnoreCase);

                        if (m.Success)
                        {
                            var name = m.Groups["name"].Value;
                            switch (m.Groups["type"].Value.ToUpperInvariant())
                            {
                                case "SOUND":
                                    _templates[name] = TemplateType.Sound;
                                    break;

                                case "EFFECTBUNDLE":
                                    _templates[name] = TemplateType.Effect;
                                    break;

                                case "DECAL":
                                    _templates[name] = TemplateType.Decal;
                                    break;
                            }
                        }

                        line = reader.ReadLine();
                    }
                }
            }
        }

        /// <summary>
        /// Checks if a template exists
        /// </summary>
        /// <param name="template">The template</param>
        /// <param name="expetedType">The expected type</param>
        /// <returns>
        /// Yes, if exists and type match
        /// No, if does not exist
        /// WrontType, if exist, but type does not match
        /// </returns>
        public TemplateExists Exists(string template, TemplateType expetedType)
        {
            if (template == null) throw new ArgumentNullException( nameof( template ), "Template has to be specified" );
            if (_templates.TryGetValue(template, out var type))
            {
                return type == expetedType ? TemplateExists.Yes : TemplateExists.WrongType;
            }

            return TemplateExists.No;
        }

        /// <summary>Enum if template exists</summary>
        public enum TemplateExists
        {
            Yes,
            No,
            WrongType
        }
    }
}
