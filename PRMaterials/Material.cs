﻿using System.IO;

namespace PRMaterials
{
    /// <summary>A material in material definitions</summary>
    public class Material
    {
        /// <summary>Id of the material</summary>
        public int Id { get; set; }

        /// <summary>Name</summary>
        public string? Name { get; set; }

        /// <summary>The material, that this material inherits</summary>
        public int? BaseMaterial { get; set; }

        /// <summary>Type</summary>
        /// <remarks>
        /// Ground: 0
        /// World: 1
        /// Projectiles: 2
        /// Vehicles: 3
        /// Misc: 4
        /// </remarks>
        public MaterialType Type { get; set; }

        /// <summary>Damage loss</summary>
        public decimal DamageLoss { get; set; } = MaterialDefaults.DamageLoss;

        /// <summary>Min damage loss</summary>
        public decimal MinDamageLoss { get; set; } = MaterialDefaults.MinDamageLoss;

        /// <summary>Max damage loss</summary>
        public decimal MaxDamageLoss { get; set; } = MaterialDefaults.MaxDamageLoss;

        /// <summary>Elasticity</summary>
        public decimal Elasticity { get; set; } = MaterialDefaults.Elasticity;

        /// <summary>Friction</summary>
        public decimal Friction { get; set; } = MaterialDefaults.Friction;

        /// <summary>Flag if material is water</summary>
        public bool HasWaterPhysics { get; set; } = MaterialDefaults.HasWaterPhysics;

        /// <summary>Flag if material is barbwire (deals damage when touching)</summary>
        public bool IsBarbwire { get; set; } = MaterialDefaults.IsBarbwire;

        /// <summary>Flag if material is one sided</summary>
        public bool IsOneSided { get; set; } = MaterialDefaults.IsOneSided;

        /// <summary>Flag if material is see through</summary>
        public bool IsSeeThrough { get; set; } = MaterialDefaults.IsSeeThrough;

        /// <summary>Flag if material should always be penetrated even by materials that dont penetrate</summary>
        public bool OverrideNeverPenetrate { get; set; } = MaterialDefaults.OverrideNeverPenetrate;

        /// <summary>Deviation added when penetrating</summary>
        public decimal PenetrationDeviation { get; set; } = MaterialDefaults.PenetrationDeviation;

        /// <summary>Collision hardness</summary>
        public decimal ProjectileCollisionHardness { get; set; } = MaterialDefaults.ProjectileCollisionHardness;

        /// <summary>Resistance</summary>
        public decimal Resistance { get; set; } = MaterialDefaults.Resistance;


        /// <summary>
        /// Writes this material to a con file
        /// </summary>
        /// <param name="conWriter">Writer to write con file</param>
        public void WriteToCon(TextWriter conWriter)
        {
            const string materialClass = "Material";

            // Properties that always get written
            conWriter.WriteLine($"{materialClass}.active {this.Id}");
            conWriter.WriteLine($"{materialClass}.name \"{this.Name}\"");
            conWriter.WriteLine($"{materialClass}.type {this.Type:D}");
            conWriter.WriteLine($"{materialClass}.friction {this.Friction:0.####}");
            conWriter.WriteLine($"{materialClass}.elasticity {this.Elasticity:0.####}");
            conWriter.WriteLine($"{materialClass}.resistance {this.Resistance:0.####}");

            // Only written when not default
            if (this.ProjectileCollisionHardness != MaterialDefaults.ProjectileCollisionHardness) conWriter.WriteLine($"{materialClass}.projectileCollisionHardness {this.ProjectileCollisionHardness:0.####}");
            if (this.DamageLoss != MaterialDefaults.DamageLoss) conWriter.WriteLine($"{materialClass}.damageLoss {this.DamageLoss:0.####}");
            if (this.MinDamageLoss != MaterialDefaults.MinDamageLoss) conWriter.WriteLine($"{materialClass}.minDamageLoss {this.MinDamageLoss:0.####}");
            if (this.MaxDamageLoss != MaterialDefaults.MaxDamageLoss) conWriter.WriteLine($"{materialClass}.maxDamageLoss {this.MaxDamageLoss:0.####}");
            if (this.PenetrationDeviation != MaterialDefaults.PenetrationDeviation) conWriter.WriteLine($"{materialClass}.penetrationDeviation {this.PenetrationDeviation:0.####}");
            if (this.OverrideNeverPenetrate != MaterialDefaults.OverrideNeverPenetrate) conWriter.WriteLine($"{materialClass}.overrideNeverPenetrate {FormatBool(this.OverrideNeverPenetrate)}");
            if (this.HasWaterPhysics != MaterialDefaults.HasWaterPhysics) conWriter.WriteLine($"{materialClass}.hasWaterPhysics {FormatBool(this.HasWaterPhysics)}");
            if (this.IsBarbwire != MaterialDefaults.IsBarbwire) conWriter.WriteLine($"{materialClass}.isBarbwire {FormatBool(this.IsBarbwire)}");
            if (this.IsOneSided != MaterialDefaults.IsOneSided) conWriter.WriteLine($"{materialClass}.isOneSided {FormatBool(this.IsOneSided)}");
            if (this.IsSeeThrough != MaterialDefaults.IsSeeThrough) conWriter.WriteLine($"{materialClass}.isSeeThrough {FormatBool(this.IsSeeThrough)}");

            // Method to convert Bool to 1 or 0
            string FormatBool(bool value)
            {
                return value ? "1" : "0";
            }
        }
    }
}
